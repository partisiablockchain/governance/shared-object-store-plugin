package com.partisiablockchain.governance.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.blockchain.BlockchainSharedObjectStorePlugin;
import com.partisiablockchain.blockchain.PluginContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.SafeDataInputStream;

/** Shared object store on-chain plugin, allows storing binary objects across all shards. */
@Immutable
public final class SharedObjectStore
    extends BlockchainSharedObjectStorePlugin<SharedObjectStoreGlobal> {
  @Override
  public boolean exists(SharedObjectStoreGlobal globalState, Hash hash) {
    return globalState.containsKey(hash);
  }

  @Override
  public Class<SharedObjectStoreGlobal> getGlobalStateClass() {
    return SharedObjectStoreGlobal.class;
  }

  @Override
  public InvokeResult<SharedObjectStoreGlobal> invokeGlobal(
      PluginContext pluginContext, SharedObjectStoreGlobal state, byte[] rpc) {
    return new InvokeResult<>(SafeDataInputStream.readFully(rpc, state::invoke), null);
  }

  @Override
  public SharedObjectStoreGlobal migrateGlobal(
      StateAccessor stateAccessor, SafeDataInputStream rpc) {
    if (stateAccessor == null) {
      return SharedObjectStoreGlobal.create();
    } else {
      return SharedObjectStoreGlobal.create(stateAccessor);
    }
  }
}
