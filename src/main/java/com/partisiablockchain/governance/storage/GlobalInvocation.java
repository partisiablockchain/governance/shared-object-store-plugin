package com.partisiablockchain.governance.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;

/** Invocations for the global shared object store state. */
public enum GlobalInvocation {
  /** Add a new binary object to the global state of the shared object store plugin. */
  ADD_SHARED_OBJECT {
    @Override
    SharedObjectStoreGlobal invoke(SharedObjectStoreGlobal global, SafeDataInputStream rpc) {
      byte[] data = rpc.readDynamicBytes();
      return global.add(data);
    }
  },
  /** Remove a binary object from the global state of the shared object store plugin. */
  REMOVE_SHARED_OBJECT {
    @Override
    SharedObjectStoreGlobal invoke(SharedObjectStoreGlobal global, SafeDataInputStream rpc) {
      Hash hash = Hash.read(rpc);
      return global.remove(hash);
    }
  };

  abstract SharedObjectStoreGlobal invoke(SharedObjectStoreGlobal global, SafeDataInputStream rpc);
}
