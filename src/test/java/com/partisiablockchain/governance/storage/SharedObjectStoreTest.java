package com.partisiablockchain.governance.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SharedObjectStoreTest {
  private final SharedObjectStoreGlobal globalState = SharedObjectStoreGlobal.create();
  private final SharedObjectStore subject = new SharedObjectStore();

  @Test
  void readRawNullValue() {
    boolean exists = subject.exists(globalState, Hash.create(s -> s.writeString("foo")));
    Assertions.assertThat(exists).isFalse();
  }

  @Test
  void readRawValue() {
    byte[] data = {1, 2, 3, 4};
    SharedObjectStoreGlobal state = globalState.add(data);
    Hash identifier = new LargeByteArray(data).getIdentifier();
    Assertions.assertThat(subject.exists(state, identifier)).isTrue();
  }

  @Test
  void getGlobalStateClass() {
    Assertions.assertThat(subject.getGlobalStateClass()).isEqualTo(SharedObjectStoreGlobal.class);
  }

  @Test
  void addStorageObject() {
    byte[] data = {101, 102, 100, -1, 60, 61, 62, 63};
    Hash hash = new LargeByteArray(data).getIdentifier();
    Assertions.assertThat(subject.exists(globalState, hash)).isFalse();

    SharedObjectStoreGlobal updatedState =
        subject
            .invokeGlobal(
                null,
                globalState,
                SafeDataOutputStream.serialize(
                    stream -> {
                      stream.writeEnum(GlobalInvocation.ADD_SHARED_OBJECT);
                      stream.writeDynamicBytes(data);
                    }))
            .updatedState();

    Assertions.assertThat(subject.exists(updatedState, hash)).isTrue();
  }

  @Test
  void removeStorageObject() {
    byte[] data = {1, 2, 3, 4};
    SharedObjectStoreGlobal state = globalState.add(data);
    Hash identifier = new LargeByteArray(data).getIdentifier();
    Assertions.assertThat(subject.exists(state, identifier)).isTrue();

    SharedObjectStoreGlobal updatedState =
        subject
            .invokeGlobal(
                null,
                state,
                SafeDataOutputStream.serialize(
                    stream -> {
                      stream.writeEnum(GlobalInvocation.REMOVE_SHARED_OBJECT);
                      identifier.write(stream);
                    }))
            .updatedState();

    Assertions.assertThat(subject.exists(updatedState, identifier)).isFalse();
  }

  @Test
  void migrateGlobal() {
    final byte[] firstDataBlob = {4, 5, 1, 2, 6, 7, 8, 0, 9, 12, 61, 89, 102, 121, 89};
    final Hash firstHash = new LargeByteArray(firstDataBlob).getIdentifier();
    final byte[] secondDataBlob = {101, 102, 100, -1, 60, 61, 62, 63};
    final Hash secondHash = new LargeByteArray(secondDataBlob).getIdentifier();
    final byte[] thirdDataBlob = {1, 2, 3, 4};
    final Hash thirdHash = new LargeByteArray(thirdDataBlob).getIdentifier();

    SharedObjectStoreGlobal initial =
        subject.migrateGlobal(null, SafeDataInputStream.createFromBytes(new byte[0]));
    Assertions.assertThat(initial.get(firstHash)).isNull();
    Assertions.assertThat(initial.get(secondHash)).isNull();
    Assertions.assertThat(initial.get(thirdHash)).isNull();

    SharedObjectStoreGlobal updatedGlobal =
        initial.add(firstDataBlob).add(secondDataBlob).add(thirdDataBlob);
    Assertions.assertThat(updatedGlobal.get(firstHash)).isEqualTo(firstDataBlob);
    Assertions.assertThat(updatedGlobal.get(secondHash)).isEqualTo(secondDataBlob);
    Assertions.assertThat(updatedGlobal.get(thirdHash)).isEqualTo(thirdDataBlob);

    StateAccessor accessor = StateAccessor.create(updatedGlobal);
    SharedObjectStoreGlobal migratedGlobal =
        subject.migrateGlobal(accessor, SafeDataInputStream.createFromBytes(new byte[0]));
    Assertions.assertThat(migratedGlobal.get(firstHash)).isEqualTo(firstDataBlob);
    Assertions.assertThat(migratedGlobal.get(secondHash)).isEqualTo(secondDataBlob);
    Assertions.assertThat(migratedGlobal.get(thirdHash)).isEqualTo(thirdDataBlob);
  }
}
