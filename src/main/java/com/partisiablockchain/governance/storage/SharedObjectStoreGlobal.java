package com.partisiablockchain.governance.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;

/** The global state of the shared object store plugin. */
@Immutable
public final class SharedObjectStoreGlobal implements StateSerializable {

  private final AvlTree<Hash, LargeByteArray> storage;

  /** Construct uninitialized {@link SharedObjectStoreGlobal} for serialization. */
  @SuppressWarnings("unused")
  public SharedObjectStoreGlobal() {
    this.storage = null;
  }

  private SharedObjectStoreGlobal(AvlTree<Hash, LargeByteArray> storage) {
    this.storage = storage;
  }

  /**
   * Create a new empty global state for the shared object store plugin.
   *
   * @return new empty global state
   */
  public static SharedObjectStoreGlobal create() {
    return new SharedObjectStoreGlobal(AvlTree.create());
  }

  /**
   * Create a new global state for the shared object store plugin, using the data supplied in the
   * state accessor.
   *
   * @param stateAccessor to create new state from
   * @return new global state
   */
  public static SharedObjectStoreGlobal create(StateAccessor stateAccessor) {
    AvlTree<Hash, LargeByteArray> migratedStorage =
        stateAccessor.get("storage").typedAvlTree(Hash.class, LargeByteArray.class);
    return new SharedObjectStoreGlobal(migratedStorage);
  }

  /**
   * Add a new binary object to store on all shards.
   *
   * @param value binary object to store
   * @return updated global state containing new object
   */
  public SharedObjectStoreGlobal add(byte[] value) {
    LargeByteArray wrappedData = new LargeByteArray(value);
    return new SharedObjectStoreGlobal(storage.set(wrappedData.getIdentifier(), wrappedData));
  }

  /**
   * Remove a binary object from the store.
   *
   * @param hash identifier of the object to remove
   * @return updated global state with the object removed
   */
  public SharedObjectStoreGlobal remove(Hash hash) {
    return new SharedObjectStoreGlobal(storage.remove(hash));
  }

  /**
   * Get the data associated with the hash identifier. Returns null if no such data can be found.
   *
   * @param identifier to get the data for
   * @return data or null
   */
  public byte[] get(Hash identifier) {
    LargeByteArray value = storage.getValue(identifier);
    if (value == null) {
      return null;
    } else {
      return value.getData();
    }
  }

  /**
   * Check if the storage contains a mapping for the given key.
   *
   * @param identifier to check for
   * @return true if storage contains key, false otherwise
   */
  public boolean containsKey(Hash identifier) {
    return storage.containsKey(identifier);
  }

  /**
   * Invoke a command on the shared object store plugin.
   *
   * @param rpc invocation
   * @return new global state for the shared object store plugin
   */
  public SharedObjectStoreGlobal invoke(SafeDataInputStream rpc) {
    return rpc.readEnum(GlobalInvocation.values()).invoke(this, rpc);
  }
}
